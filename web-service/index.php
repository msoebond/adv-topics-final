<?php
header("Access-Control-Allow-Origin: *");
cors();

/**
 *  An example CORS-compliant method.  It will allow any GET, POST, or OPTIONS requests from any
 *  origin.
 *
 *  In a production environment, you probably want to be more restrictive, but this gives you
 *  the general idea of what is involved.  For the nitty-gritty low-down, read:
 *
 *  - https://developer.mozilla.org/en/HTTP_access_control
 *  - http://www.w3.org/TR/cors/
 *
 */
function cors() {

    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

    //echo "You have CORS!";
}
// Pre-requisites:
// 	1. Url Re-writing (mod rewrite)
//	2. AJAX - We should have completed the Abstracting AJAX project
//	3. Regular expressions


///////////////////////////////////
// Set up the data access object
///////////////////////////////////
if($_SERVER['SERVER_NAME'] == "localhost"){
    define("DB_HOST", "localhost");
    define("DB_USER", "root");
    define("DB_PASSWORD", "");
    define("DB_NAME", "testgamedb");
}else{
    define("DB_HOST", "localhost");
    define("DB_USER", "okinters_fred");
    define("DB_PASSWORD", "2bxqmSNGyIa!");
    define("DB_NAME", "okinters_gamesdb");
}

$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
require("includes/GameDataAccess.inc.php");
$ga = new GameDataAccess($link);



///////////////////////////////////
// Handle the Request
///////////////////////////////////

// Gather all the information about the request
$url_path = isset($_GET['url_path']) ? $_GET['url_path'] : ""; // This is the path part of the URL being requested (see the .htaccess file)
$method = $_SERVER['REQUEST_METHOD'];
$request_body = file_get_contents('php://input'); // note that GET requests do not have request body
$request_headers = getallheaders();


// This IF statement will check to see if the requested URL (and method) is supported by this web service
// If so, then we need to formulate the proper response, if not then we return a 404 status code
if($method == "GET" && empty($url_path)){

 	//Show the home page for this web service
  require("api-docs.php");
  die();

}else if($method == "GET" && ($url_path == "games/" || $url_path == "games")){

  get_all_games();

}else if($method == "POST" && ($url_path == "games/" || $url_path == "games")){
  	
  insert_game();
  
}else if($method == "GET" && preg_match('/^games\/([0-9]*\/?)$/', $url_path, $matches)){
   
  // Get the id of the game from the regular expression
  $game_id = $matches[1];
  get_game_by_id($game_id);

}else if($method == "PUT" && preg_match('/^games\/([0-9]*\/?)$/', $url_path, $matches)){

  // Get the id of the game from the regular expression
  //$game_id = $matches[1]; // we can get the game id from the request body
  update_game();  
  
}else if ($method == "GET" && ($url_path == "systems/" || $url_path == "systems")) {
  get_all_systems();
}else{

  header('HTTP/1.1 404 Not Found', true, 404);
  die("We're sorry, we can't find this page: {$_SERVER['REQUEST_URI']}");

}


/////////////////////////////////
// FUNCTIONS
/////////////////////////////////

function get_all_games(){

  global $ga, $url_path, $method, $request_body, $request_headers;
  
  // check to see if we need to sort the games by author or title
  $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : null;
  
  // get the games from the database
  $games = $ga->get_all_games($order_by);
  if(!$games){
    header('HTTP/1.1 500 server error (fetching games from the database)', true, 500);
    die();
  }
    
  // We'll default to returning games in JSON format unless the client
  // requests XML (the Accept header in the request would be set to application/xml)
  $return_format = $request_headers['Accept'];
  
  if($return_format == "application/xml"){
    //header("Content-Type","application/xml");
    die("TODO: convert game data to XML");
  }else{
    //header("Content-Type","application/json");
    $json = json_encode($games);
    echo($json);
    die();
  }

}


function insert_game(){

  global $ga, $url_path, $method, $request_body, $request_headers;

  // The data for the game being inserted should be in the request body.
  // The data should be sent in the JSON format which means that the Content-Type header in the request SHOULD be set to application/json
  // But if it's not set properly, we'll just assume the data is coming in as JSON
  // In the future, we might also accept the data in the request body to be XML, which would mean that the Content-Type header is set to application/xml
  
  $new_game = null;
  
  if($request_headers['Content-Type'] == "application/xml"){
    die("TODO: convert XML game data into an associative array");    
  }else{
    // convert the json to an associative array
    // note: json_decode() will return false if it can't convert the request body (maybe because it's not valid json)
    $new_game = json_decode($request_body, true);
  }

  // TODO: validate the $new_game assoc array (make sure it has id, title and author keys)

  if($new_game){
    
    if($new_game = $ga->insert_game($new_game)){
      // note that the game returned by insert_game() will have the id set (by the database auto increment)
      // TODO: if the Accept header in the request is set to application/xml, then the client want the data returned in XML, we could deal with that later  
      // For now, we'll just return the data in JSON format
      //header("Content-Type","application/json");
      die(json_encode($new_game));
    }else{
      header('HTTP/1.1 500 server error (fetching games from the database)', true, 500);
      die();  
    }
  
  }else{
    header('HTTP/1.1 400 - the data sent in the request is not valid', true, 400);
    die();
  }

}


function get_game_by_id($id){

  global $ga, $url_path, $method, $request_body, $request_headers;

  $game = $ga->get_game_by_id($id);

  // TODO: we may want to check the Accept header in the request, and if it's set to application/xml, we might return the data as XML instead of JSON
  // But for now, we'll just return the game data as JSON
  
  if($game){
    //header("Content-Type","application/json");
    die(json_encode($game));
  }else{
    header('HTTP/1.1 400 - the game id in the requested url is not in the database', true, 400);
    die();
  }

}


function update_game(){

  global $ga, $url_path, $method, $request_body, $request_headers;

  if($game = json_decode($request_body, true)){

    $game = $ga->update_game($game);

    if($game){
      //header("Content-Type","application/json");
      die(json_encode($game));
    }else{
      header('HTTP/1.1 500 - Unable to update game in the database', true, 500);
      die();
    }

  }else{
    header('HTTP/1.1 400 - Invalid game data in the request body', true, 400);
    die();
  }

}

function get_all_systems(){
  global $ga, $url_path, $method, $request_body, $request_headers;
  

  // get the systems from the database
  $systems = $ga->get_all_systems();
  if(!$systems){
    header('HTTP/1.1 500 server error (fetching systems from the database)', true, 500);
    die();
  }
    
  // We'll default to returning systems in JSON format unless the client
  // requests XML (the Accept header in the request would be set to application/xml)
  $return_format = $request_headers['Accept'];
  
  if($return_format == "application/xml"){
    //header("Content-Type","application/xml");
    die("TODO: convert game data to XML");
  }else{
    //header("Content-Type","application/json");
    $json = json_encode($systems);
    echo($json);
    die();
  }

}
?>