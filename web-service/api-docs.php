<!DOCTYPE html>
<html>
<head>
	<title>Game Service API Docs</title>
</head>
<body>

<h1>Official API Docs for the Games Web Service</h1>
  <table border="1">
    <tr>
      <th>API CALL (ROUTE)</th>
      <th>METHOD</th>
      <th>ACTION</th>
    </tr>
    <tr>
      <td>games/</td>
      <td>GET</td>
      <td>Get's all games</td>
    </tr>
    <tr>
      <td>games/</td>
      <td>POST</td>
      <td>
        Inserts a new game into the data base <br>
        Expects the Content-Type to be application/json
      </td>
    </tr>
    <tr>
      <td>games/x</td>
      <td>GET</td>
      <td>
        Get's a game by it's id property <br>
        ex: <b>games/1</b> would fetch the game with an id of 1
      </td>
    </tr>
    <tr>
      <td>games/x</td>
      <td>PUT</td>
      <td>
        Edits the game with id of x <br>
        Note: then Content-Type should be application/json
      </td>
    </tr>
    <tr>
      <td>systems/</td>
      <td>GET</td>
      <td>Get's all the systems. There is no way to add new systems</td>
    </tr>
  </table>
  ***By default, responses will return data in JSON format <br>

</body>
</html>