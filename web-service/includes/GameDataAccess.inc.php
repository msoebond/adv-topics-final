

<?php
class GameDataAccess{
	private $link;

	function __construct($link){
		$this->link = $link;
	}

	function get_all_games($order_by = null){
		$qStr = "SELECT	
			games.game_id , 
			games.game_name, 
			games.game_common_name,
			games.game_desc,
			games.game_active,
			systems.system_name AS system 
			FROM games
			LEFT JOIN systems
			ON games.game_system = systems.system_id";

		if($order_by == "system"){
			$qStr .= " ORDER BY " . $order_by;
		}
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_games = array();

		while($row=mysqli_fetch_assoc($result)){
			$game = array();
			$game['id'] = htmlentities($row['game_id']);
			$game['name'] = htmlentities($row['game_name']);
			$game['common_name'] = htmlentities($row['game_common_name']);
			$game['desc'] = htmlentities($row['game_desc']);
			$game['active'] = htmlentities($row['game_active']);
			$game['system'] = htmlentities($row['system']);

			$all_games[] = $game;
		}

		return $all_games;
	}

	function get_game_by_id($id){
		$qStr = "SELECT	
			games.game_id , 
			games.game_name, 
			games.game_common_name,
			games.game_desc,
			games.game_active,
			games.game_system,
			systems.system_name AS system 
			FROM games
			LEFT JOIN systems
			ON games.game_system = systems.system_id
			WHERE games.game_id=" . mysqli_real_escape_string($this->link, $id);

				
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);

			$game = array();
			$game['id'] = htmlentities($row['game_id']);
			$game['name'] = htmlentities($row['game_name']);
			$game['common_name'] = htmlentities($row['game_common_name']);
			$game['desc'] = htmlentities($row['game_desc']);
			$game['active'] = htmlentities($row['game_active']);
			$game['game_system'] = htmlentities($row['game_system']);
			$game['system'] = htmlentities($row['system']);

			//die(implode(",",$game));
			return $game;

		}else{
			return null;
		}
	}

	function insert_game($game){

		// prevent SQL injection
		$game['name'] = mysqli_real_escape_string($this->link, $game['name']);
		$game['common_name'] = mysqli_real_escape_string($this->link, $game['common_name']);
		$game['desc'] = mysqli_real_escape_string($this->link, $game['desc']);
		$game['active'] = (strtoupper(mysqli_real_escape_string($this->link, $game['active']))=="YES")?"YES":"NO";
		//$game['system'] = mysqli_real_escape_string($this->link, $game['system']);
		//$system = get_system_by_name($game['system']);
		//$game['game_system'] = $system['system_id'];


		/*
		$result = $this->get_system_by_name($game['system']) OR $this->handle_error("No system found");
		if($result->num_rows == 1){
			$system = mysqli_fetch_assoc($result);
			$system_id = $system['system_id'] || 0;
		}else{
			$system_id = 0;
		}
		*/
		


		$qStr = "INSERT INTO games (
					game_name,
					game_common_name,
					game_desc,
					game_active,
					game_system
				) VALUES (
					'{$game['name']}',
					'{$game['common_name']}',
					'{$game['desc']}',
					'{$game['active']}',
					'{$game['game_system']}'
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the game id that was assigned by the data base
			$game['id'] = mysqli_insert_id($this->link);
			// then return the game
			return $game;
		}else{
			$this->handle_error("unable to insert game");
		}

		return false;
	}

	function update_game($game){

		// prevent SQL injection
		$game['id'] = mysqli_real_escape_string($this->link, $game['id']);
		$game['name'] = mysqli_real_escape_string($this->link, $game['name']);
		$game['common_name'] = mysqli_real_escape_string($this->link, $game['common_name']);
		$game['desc'] = mysqli_real_escape_string($this->link, $game['desc']);
		$game['active'] = mysqli_real_escape_string($this->link, $game['active']);
		//$game['system'] = mysqli_real_escape_string($this->link, $game['system']);
		//$system = get_system_by_name($game['system']);
		//$game['game_system'] = $system['system_id'];

		$qStr = "UPDATE games SET 
			game_name='" . $game['name'] . "', 
			game_common_name='" . $game['common_name'] . "',
			game_desc='". $game['desc'] . "',
			game_active='". $game['active'] . "',
			game_system='" . $game['game_system'] . "' 
			WHERE game_id = " . $game['id'];
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $game;
		}else{
			$this->handle_error("unable to update game");
		}

		return false;
	}

	function get_all_systems(){
		$qStr = "SELECT
			system_id,
			system_name,
			system_icon_filename
			FROM systems
		";

		$result = mysqli_query($this->link, $qStr) or $this->handle_error("mysqli_error($this->link)");

		$all_systems = array();

		while($row=mysqli_fetch_assoc($result)){
			$system = array();
			$system['system_id'] = htmlentities($row['system_id']);
			$system['name'] = htmlentities($row['system_name']);
			$system['system_icon_filename'] = htmlentities($row['system_icon_filename']);

			$all_systems[] = $system;
		}

		return $all_systems;
	}



	function get_system_by_name($name){
		$name = mysqli_real_escape_string($this->link, $name);
		$qStr = "SELECT
			system_id,
			system_name,
			system_icon_filename
			FROM systems
			WHERE system_name='". $name . "'";

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link)); 

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);
			$system = array();
			$system['system_id'] = htmlentities($row['system_id']);
			$system['system_name'] = htmlentities($row['system_name']);
			$system['system_icon_filename'] = htmlentities($row['system_icon_filename']);
			return $system;
		}else{
			return null;
		}
	}

	function get_system_by_id($id){
		$qStr = "SELECT 
			system_id,
			system_name,
			system_icon_filename
			FROM  systems
			WHERE system_id=" . mysqli_real_escape_string($this->link, $name);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($resul->num_rows == 1){
			$row = mysqli_fetch_assoc($result);
			$system = array();
			$system['system_id'] = htmlentities($row['system_id']);
			$system['system_name'] = htmlentities(($row['system_name']));
			$system['system_icon_filename'] = htmlentities(($row['system_icon_filename']));

			return $system;
		}else{
			return null;
		}
	}

	function handle_error($msg){
		echo("<strong>GAME DATABASE ERROR: </strong>" . $msg);
	}
	
}



?>