 Final Project

Posted on: Monday, November 20, 2017 7:52:36 AM CST

Hi everyone,

Here are the details of the final project per our discussion in class yesterday. It looks like a lot, but I wanted to be as specific as possible. We'll have time to work on the project each week in class. We'll also start to learn about NodeJS next week (which we'll use to deploy the app).

You can go ahead and get started on the database and web service. But it would be ideal to finish the book list and book details modules before starting on the front end (you'll be much more prepared, and less likely to make a mess of the code).

Single Page CRUD App and Web Service

Create a single page CRUD app and web service. Make sure to create a new Git repository for the project. And make sure to commit your changes on a regular/daily basis. You will also have to provide API docs for both the single page web app, and the web service.

CRUD stands for create, retrieve, update, delete. So your app should allow users to perform these actions on the database.

Step 1 - Design the Database

Start by designing and creating a database in MySQL that has at least one table. The table must have at least 5 columns in it. Give the table an appropriate name.

We won't actually delete any rows from the table(s), instead we'll do 'virtual deletes'. So your table(s) should have a column that indicates if the row has been 'virtually deleted'. We used an 'active' column in Adv. Web., and we made the data type an enum that can be 'yes' or 'no'.

Step 2 - Build the Web Service

Build a web service that allows HTTP requests to run CRUD operations against the database. You may use the book-web-service as a template. The web service should include API calls that do the following:

    1. CREATE - This will insert a row into your table, it should be a POST request that has JSON data in the request body. The JSON data will be inserted into the database.
    2. RETRIEVE - This will fetch all 'active' rows from the table, it should be a GET request. The response should be a JSON array.
    3. UPDATE and DELETE - This will edit a row (and may 'virtually delete' it), it should be a PUT request that has JSON data in the body. The JSON data will be used to update a row in the database.

Step 3 - Build the Front End

The single page web app that you create should include the following modules (each in it's own .js file)

    1. A module for doing ajax calls (you can use the one that you've already created).
    2. A data access module (which uses the ajax module to to send data to the web service, and to fetch data from the web service).
    3. A list module that displays all active rows from the table. The list module should allow users to select an item to edit.
    4. A details module that displays all the details of each row from the database (although you should hide the id from the user). This module should allow for inserts, updates, and deletes

You must design the user interface to allow new rows to be inserted into the database as well.

The list and details modules should share a .css file that controls their appearances.

The modules should be as 'loosely coupled' as possible.

Make sure to use a unique 'namespace ' for your modules in order to prevent name collisions (which can corrupt the global namespace).

You should use all the best practices of web development that we covered in the program (for example, all img tags should have an 'alt' attribute set, the content displayed to the user should be well-written, etc.)

Step 4 - Deploying the App (Don't worry, I'll help you set all this stuff up)

Use NodeJS to do the following:

    1. Minify and concatenate all .js files
    2. Compile .sass files into .css files
    3. Concatenate and minify .css files
    4. Generate API docs for the front end (the web application)
    5. Deploy the appto your live web server

To submit the project:

    1. Attach the URL to the live site to the assignment in Blackboard
    2. Attach the URL to the BitBucket repository to the assignment in Blackboard

