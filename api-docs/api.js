YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "GameDataAccess",
        "GameDetails",
        "GameList"
    ],
    "modules": [
        "OKGame"
    ],
    "allModules": [
        {
            "displayName": "OKGame",
            "name": "OKGame",
            "description": "This is the module that set everything up.\nRequires OKGame.GameDataAccess, OKGame.GameDetails, and OKGame.GameList"
        }
    ],
    "elements": []
} };
});