var acme = acme || {};

acme.ajax = {
	send: function(options){

		var http = new XMLHttpRequest();
		var callback = options.callback;
		var url = options.url;
		var method = options.method || "GET";
		var headers = options.headers || {};
		var requestBody = options.requestBody || "Empty body.";
		var errorCallback = options.errorCallback;


		if(method == "GET"){
			http.open("GET", url);
			setHttpProperties();
			http.send();
		}else if (method == "POST"){
			http.open("POST", url);
			setHttpProperties();
			http.send(requestBody);
		}else if (method == "PUT"){
			http.open("PUT", url)
			setHttpProperties();
			http.send(requestBody);
		}

		function setHttpProperties(){
			for(property in headers){
				http.setRequestHeader(property, headers[property]);
			}			
		}






		
		

		http.onreadystatechange = function(){
		// send the XmlHttpRequest, and if the http.readyState == 4 and the http.status == 200
		// then invoke the callback function and pass the responseText as a param to it			
			if(http.readyState == 4 && http.status == 200){
				callback(http.responseText);
		// if the http.readyState == 4 and the http.status is NOT 200 then invoke the error method (defined below)
		// and pass the status as a param, like this...
		//this.error(http.status);				
			}else if(http.readyState == 4){
				if(errorCallback){
					errorCallback(http.status);
				}else{
					acme.ajax.error(http.status);
				}
			}		
		}

		


		


	},
	error: function(errMsg){
		alert(errMsg);
	}
};