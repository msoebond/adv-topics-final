var OKGame = OKGame || {};

/**
 * Fills a div with a list of all games
 * 
 * @param options
 * @param options.target container for list
 * @param options.games {array} array of game objects to fill the list
 * @param options.onclick {function} function to call when edit button is clicked
 * @class GameList
 * @constructor init
 */
OKGame.GameList = {
	init: function(options){
		var target = options.target;
		var games = options.games || null;
		var onclick = options.onclick;

		if(target instanceof Element == false){
			// wasn't given a valid target to put list;
			return;
		}

		// do everyting
		makeList();


		
		/**
		 * Creates/recreates the list
		 * 
		 * @param {object} options
		 * @param {array} options.games changes the games array
		 * @optional
		 * @param {function} options.onclick changes the onclick function
		 * @optional
		 * @method makeList
		 */
		function makeList(options){
			if(options){
				if(options.games){
					games = options.games;
				}
				if(options.onclick){
					onclick = options.onclick;
				}
			}

			emptyGameList();
			var ul = makeUL();
			if(onclick){
				ul.addEventListener("click", onclick);
			}
			ul.setAttribute("class", "gameList");
			hd = document.createElement("h1");
			hd.setAttribute("class", "title");
			hd.innerHTML = "All Games";
			target.appendChild(hd);
			target.appendChild(ul);
		}
		

		function emptyGameList(){
			target.innerHTML = "";
		}

		function makeUL(){
			var ul = document.createElement("ul");
			ul.setAttribute("class", "gameList");
			for(gamePos in games){
				var li = makeGameElement({
					id: 			games[gamePos].id,
					name: 			games[gamePos].name,
					common_name: 	games[gamePos].common_name,
					desc: 			games[gamePos].desc,
					active: 		games[gamePos].active,
					system: 		games[gamePos].system
				});
				li.setAttribute("class", "gameListItem");
				ul.appendChild(li);
			}
			ul.addEventListener("click", onclick);

			return ul;
		}

		function makeGameElement(options){
			var id = options.id;
			var name = options.name;
			var common_name = options.common_name;
			var desc = options.desc;
			var active = options.active;
			var system = options.system;
			var btn;

			var li = document.createElement("li");
			btn = makeButton({
				value: "Edit",
				dataId: id
			});
			li.appendChild(btn);
			li.appendChild(document.createTextNode(
				" Name: " + name +
				", Aka: " + common_name +
				", Description: " + desc +
				", System: " + system));

			
			return li;
		}

		function makeButton(options){
			var value = options.value || "Click Me";
			//var onclick = options.onclick;
			var dataId = options.dataId;
			var btn = document.createElement("input");
			btn.type = "button";
			btn.setAttribute("value", String(value));
			btn.setAttribute("dataId", String(dataId));
			

			return btn;
		}

		
 
	 	// end of GameList.init
	 	return {
	 		makeList: makeList
		} 
 	}
};