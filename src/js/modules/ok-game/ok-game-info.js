var OKGame = OKGame || {};
/**
 * This is the module that set everything up.
 * Requires OKGame.GameDataAccess, OKGame.GameDetails, and OKGame.GameList
 *
 * @param {String} id The name of the container
 * @module OKGame
 */
OKGame.OKGameInfo = function(id){
	var gda = OKGame.GameDataAccess();
	var container = document.getElementById(id);
	var GLDiv= document.createElement("div");
	var GDDiv = document.createElement("div");
	container.appendChild(GDDiv);
	container.appendChild(GLDiv);
	var gArray;
	var gamesList;
	var systemList;
	var sArray;
	var GL;
	var GD;
	gda.getAllSystems(function(systemList){
	  sArray = JSON.parse(systemList);

	  gda.getAllGames(function(gamesList){
	    gArray = JSON.parse(gamesList);

	    /*
	    for(var i = 0; i < gArray.length; i++){
	      var gameItem = document.createTextNode(gArray[i].name);
	      GLDiv.appendChild(gameItem);
	      GLDiv.appendChild(document.createElement("br"));
	    }
	    */
	    GD = OKGame.GameDetails.init({
	      target: GDDiv,
	      systems: sArray,
	      onclick: (function(){
	        var game = GD.getCurrentGame();
	        if(game.id > 0){
	          gda.replaceGame(game, reset);
	        }else{
	          gda.addGame(game, reset);
	        }

	        function reset(){
	          gda.getAllGames(function(gamesList){
	            gArray = JSON.parse(gamesList);
	            GD.resetCurrentGame();
	            GL.makeList({games: gArray});
	          });
	          
	        }
	      })
	    });
	    GL = OKGame.GameList.init({
	      target: GLDiv,
	      games: gArray,
	      onclick: (function(evt){
	        id = evt.target.getAttribute("dataId");
	        if(isNaN(id) || id == null){
	          // Not valid, do nothing
	        }else{
	          loadGame(id);
	        }
	      })
	    });

	  });
	});

	function loadGame(id){
	  gda.getGameById(id, (function(game){  
	    GD.setCurrentGame(JSON.parse(game));
	    //console.log(GD.getCurrentGame());
	  }));
	}
};