var OKGame = OKGame || {};

/**
 * This class fills the details section
 *
 * @param {object} options
 * @param options.target the container for this
 * @param options.systems array of system objects {system_id: database id number, name: system name}
 * @param options.onclick function to run when save is hit
 * @class GameDetails
 * @constructor init
 */
OKGame.GameDetails = {
	init: function(options){
		var target = options.target;
		var systems = options.systems;
		var onclick = options.onclick;
		if(target instanceof Element == false){
			// wasn't given a valid target to put list;
			return;
		}

		var game;
		var txtId;
		var txtName;
		var txtCommonName;
		var taDesc;
		var ckActive;
		var selSystem;

		makeDetails();


		function makeDetails(options){
			if(options){
				if(options.game){
					game = options.game;
				}
			}
			createDetails();
		}

		function createDetails(){
			hd = document.createElement("h1");
			hd.setAttribute("class", "title");
			hd.innerHTML = "Game Details";

			txtId = document.createElement("input");
			txtId.type = "hidden";
			txtName = document.createElement("input");
			txtName.type = "text";
			txtCommonName = document.createElement("input");
			txtCommonName.type = "text";
			taDesc = document.createElement("textarea");
			ckActive = document.createElement("input");
			ckActive.type = "checkbox";
			selSystem = document.createElement("select");
			populateSystemList(systems);

			target.appendChild(hd);
			
			target.appendChild(txtId);
			target.appendChild(document.createTextNode("Name: "));
			target.appendChild(txtName);
			target.appendChild(document.createTextNode("AKA: "));
			target.appendChild(txtCommonName);
			target.appendChild(document.createTextNode("Description: "));
			target.appendChild(taDesc);
			target.appendChild(document.createTextNode("Active?: "));
			target.appendChild(ckActive);
			target.appendChild(document.createTextNode("System: "));
			target.appendChild(selSystem);

			target.appendChild(makeButton({value:"Save", onclick:onclick}));
			target.appendChild(makeButton({value:"Reset", onclick:resetCurrentGame}));

		}

		/**
		* Sets the game shown
		* 
		* @param {object} game if null, will reset the info
		* @optional
		* @param {int} game.id
		* @param {String} game.name
		* @param {String} game.common_name
		* @param {String} game.desc
		* @param {enum "YES", "NO"} game.active
		* @param {int} game.game_system 
		* 
		* 
		* @method setCurrentGame
		*/
		function setCurrentGame(game){
			if(game){
				txtId.value = game.id;
				txtName.value = game.name;
				txtCommonName.value = game.common_name;
				taDesc.value = game.desc;
				ckActive.checked = (game.active == "YES");
				selSystem.value = game.game_system;
			}else{
				resetCurrentGame();
			}
		}

		/**
		 * clears out the game infor
		 * 
		 * @method resetCurrentGame
		 */
		function resetCurrentGame(){
			txtId.value = "";
			txtName.value = "";
			txtCommonName.value = "";
			taDesc.value = "";
			ckActive.checked = false;
			selSystem.selectedIndex = 0;
		}

		/**
		 * returns a game object with the properties in the details area
		 * 
		 * @return {object} game
		 * @method getCurrentGame
		 */
		function getCurrentGame(){
			return {
				id: txtId.value || 0,
				name: txtName.value,
				common_name: txtCommonName.value,
				desc: taDesc.value,
				active: ((ckActive.checked)?"YES":"NO"),
				game_system: selSystem.options[selSystem.selectedIndex].value
			};
		}

		function populateSystemList(systemArray){
			selSystem.innerHTML = "";
			if(systemArray.length){
				for(var i = 0; i<systemArray.length; i++){
					var opt = document.createElement("option");
					opt.setAttribute("value", systemArray[i].system_id);
					opt.setAttribute("label", systemArray[i].name);
					selSystem.appendChild(opt);
				}
			}
		}

		function makeButton(options){
			var value = options.value || "Click Me";
			var onclick = options.onclick || null;
			var btn = document.createElement("input");
			btn.type = "button";
			btn.setAttribute("value", String(value));
			if(onclick){
				btn.onclick = onclick;
			}


			return btn;
		}

		return {
			setCurrentGame: setCurrentGame,
			getCurrentGame: getCurrentGame,
			resetCurrentGame: resetCurrentGame
		}

	}
};