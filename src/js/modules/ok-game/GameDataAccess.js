var OKGame = OKGame || {};
// requires acme.ajax

// Change this to hosted location of web service
var urlWebService;

if(location.host == "localhost"){
    urlWebService = "http://localhost/adv-topics-final/web-service/";
}else{
    urlWebService = "http://www.okinterstellar.com/web-service/";
}

/**
 * Class that uses ajax calls to get and send game information
 * 
 * @class GameDataAccess
 */
OKGame.GameDataAccess = function(){

	// Make sure that acme.ajax is loaded
	var ajax = acme.ajax;
	if(!ajax){
		throw new Error("This module depends upon the acme.ajax module!");
	}

	/**
	 * gets an array of all games
	 * 
	 * @param {function} callback
	 * @return {array} games array of game objects
	 * @method getAllGames
	 */
	function getAllGames(callback){
		acme.ajax.send({
			callback: callback,
			url: urlWebService + "games/",
			method: "GET"
		});
	}

	/**
	 * gets a specific game
	 * 
	 * @param {int} id
	 * @param {function} callback
	 * @method getGameById
	 */
	function getGameById(id, callback){
		acme.ajax.send({
			callback: callback,
			url: urlWebService + "games/" + id,
			method: "GET"
		});
	}

	/**
	 * gets an array of all games
	 * 
	 * @param {object} game new game to add
	 * @param {function} callback
	 * @method addGame
	 */
	function postGame(game, callback){
		acme.ajax.send({
			callback: callback,
			url: urlWebService + "games/",
			method: "POST",
			requestBody: JSON.stringify(game)
		});
	}

 	/**
	 * gets an array of all games
	 * 
	 * @param {object} game game object with all details; will replace everything
	 * @param {function} callback
	 * @method replaceGame
	 */
	function putGame(game, callback){
		acme.ajax.send({
			callback: callback,
			url: urlWebService + "games/" + game["id"],
			method: "PUT",
			requestBody: JSON.stringify(game)
		});
	}

	/**
	 * gets an array of all systems
	 * 
	 * @param {function} callback
	 * @return {array} systems array of system objects
	 * @method getAllSystems
	 */
	function getAllSystems(callback){
		acme.ajax.send({
			callback: callback,
			url: urlWebService + "systems/",
			method: "GET"
		});
	}


	//Public API
	return{
		getAllGames: getAllGames,
		getGameById: getGameById,
		addGame: postGame,
		replaceGame: putGame,
		getAllSystems: getAllSystems
	}


}